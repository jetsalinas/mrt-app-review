import React from 'react'
import { Header, Segment } from 'semantic-ui-react'
import { About } from './About'


//use ECMA class
//transfer padding on Segment to styles variable
export const Head = () => {
  return (
    <Segment clearing padded='very' style={styles.segment}>
        <Header as='h2' floated='left' style={styles.header}>
          MRT Application
        </Header>
        <About />
    </Segment>
  );
}

const styles = {
  segment: {
    backgroundColor: '#7b1113', //transfer to global css variable for reusability
  },
  header: {
    color: 'white', //use #FFF or i'll be mad
    marginBottom: 0,  //use relative units
    paddingLeft: '100px', //use relative units
  },
};
